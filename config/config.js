/* Magic Mirror Config Sample
 *
 * By Michael Teeuw http://michaelteeuw.nl
 * MIT Licensed.
 */

var config = {
	port: 8080,

	language: 'es',
	timeFormat: 24,
	units: 'metric',

	modules: [
		{
			module: 'alert',
		},
		{
			module: 'clock',
			position: 'top_left'
		},
		{
			module: 'calendar',
			header: 'Dias Festivos',
			position: 'top_left',
			config: {
				calendars: [
					{
						symbol: 'calendar-check-o ',
						url: 'webcal://www.calendarlabs.com/templates/ical/Mexico-Holidays.ics',
						maximumEntries: 5
					}
				]
			}
		},
		{
			module: 'compliments',
			position: 'lower_third'
		},
		{
			module: 'currentweather',
			position: 'top_right',
			config: {
				location: 'Monterrey',
				appid: 'd3b92b9d4fef0e6712bad42ca152a661'
			}
		},
		{
			module: 'weatherforecast',
			position: 'top_right',
			header: 'Pronóstico del tiempo',
			config: {
	            location: 'Monterrey',
	            appid: 'd3b92b9d4fef0e6712bad42ca152a661'
			}
		},
		{
			module: 'newsfeed',
			position: 'lower_third',
			config: {
				feeds: [
					{
						title: "El Norte",
						url: "http://www.elnorte.com/rss/portada.xml",
						encoding: "UTF-8"
					}
				],
				showSourceTitle: true,
				showPublishDate: true
			}
		},
		//{
			//module: 'custom_module',
			//position: 'top_right',
			//header: 'Pronóstico del tiempo',
			//config: {
	    //        location: 'Monterrey',
	    //        appid: 'd3b92b9d4fef0e6712bad42ca152a661'
			//}
		//},



	]

};

/*************** DO NOT EDIT THE LINE BELOW ***************/
if (typeof module !== 'undefined') {module.exports = config;}
