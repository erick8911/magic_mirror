var http = require('http');
var fs = require('fs');
var youtubedl = require('youtube-dl');
Module.register("custom_module",{
    // Default module config.
    defaults: {
      text: "Hello World!"
    },

    // Override dom generator.
    getDom: function() {
      var device_id = new Buffer("raspberry:123456").toString('base64')
      http.get({
          host: 'localhost',
          port: "3030",
          path: "/devices/connection?q=" + device_id
        }, function(response) {
        response.on('data', function(d) {
          var textChunk = d.toString('utf8');
          response = JSON.parse(textChunk)
          videos_in_system = walkSync(process.cwd());
          videos_hash = response.urls
          console.log(videos_hash)
          videos_to_download = downloadVideos(videos_hash)
          videos_to_delete = videos_in_system.filter( function( el ) {
            return videos_to_download.indexOf( el ) < 0;
          } );
          console.log(videos_to_delete)
          deleteVideos(videos_to_delete)
        });
      });
    },

    downloadVideos: function(videos_hash){
      console.log("Downloading all videos")
      videos_to_download = []
      for(var video_key in videos_hash){
        file_name = video_key + ".mp4"
        video_link = videos_hash[video_key]
        fullpath = process.cwd() + "/" + file_name
        videos_to_download.push(fullpath)
        downloadVideo(video_key, file_name, video_link);
      }
      return videos_to_download
    },
    // Delete videos
    deleteVideos: function(videos_to_delete){
      console.log("Deleting videos")
      //Delete videos
      videos_to_delete.forEach(function(video_to_delete) {
        console.log(video_to_delete)
         fs.stat(video_to_delete, function (err, stats) {
           if (err) {
            console.log("File doesnt exists")
           }

           fs.unlink(video_to_delete,function(err){
             if(!err){
              console.log('file deleted successfully');
             }
           });
        });
      });
    },

    //Download videos
    downloadVideo: function(video_name, file_name, video_link) {
      fs.access(file_name, fs.F_OK, function(err) {
          console.log("Downloadig video " + video_name)
          if (err) {
            //File Doesnt exists, download from youtube
            console.log(video_name+": "+ video_link);
            var video = youtubedl(video_link);
            video.on('info', function(info) {
              //console.log('Download started');
              console.log('filename: ' + info._filename);
              console.log('size: ' + info.size);
            });
            video.pipe(fs.createWriteStream(file_name));
          } else {
            //File exists, Do nothing
            console.log("Video Exists nothing to download")
          }
      });
    },



    // sync version
    walkSync: function(currentDirPath) {
      var videos_in_system = []
      var fs = require('fs'), path = require('path');
      fs.readdirSync(currentDirPath).forEach(function (name) {
        var filePath = path.join(currentDirPath, name);
        if (filePath.indexOf(".mp4") > 0) {
          videos_in_system.push(filePath)
        }
      });
      return videos_in_system
    },


});
